﻿using ProjetTennis.models;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace TestProject
{
    public class TennisTests
    {

        [Theory]
        [InlineData(-1, 0)]
        [InlineData(0, -1)]
        [InlineData(-1, -1)]
        [InlineData(-20, 0)]
        public void NombreInvalides(int echangesJoueur1, int echangesJoueur2) {

            TennisScore tennisScore = new TennisScore();

            Assert.Throws<Exception>(() => tennisScore.ConversionEchangesScore(echangesJoueur1, echangesJoueur2));

        }

        [Theory]
        [InlineData(0, 0, "0|0")]
        [InlineData(0, 2, "0|30")]
        [InlineData(1, 1, "15|15")]
        [InlineData(1, 3, "15|40")]
        public void ScoreSimple(int echangesJoueur1, int echangesJoueur2, string resultat) {

            TennisScore tennisScore = new TennisScore();

            Assert.Equal(resultat, tennisScore.ConversionEchangesScore(echangesJoueur1, echangesJoueur2));

        }


        [Theory]
        [InlineData(0, 4, "0|Victoire")]
        [InlineData(4, 0, "Victoire|0")]
        [InlineData(2, 4, "30|Victoire")]
        [InlineData(4, 2, "Victoire|30")]
        public void ScoreVictoire(int echangesJoueur1, int echangesJoueur2, string resultat) {

            TennisScore tennisScore = new TennisScore();

            Assert.Equal(resultat, tennisScore.ConversionEchangesScore(echangesJoueur1, echangesJoueur2));

        }

        [Theory]
        [InlineData(4, 5, "40|Av")]
        [InlineData(5, 4, "Av|40")]
        [InlineData(6, 5, "Av|40")]
        [InlineData(7, 6, "Av|40")]
        [InlineData(6, 7, "40|Av")]
        [InlineData(50, 50, "40|40")]
        [InlineData(50, 51, "40|Av")]
        [InlineData(50, 52, "40|Victoire")]

        public void ScoreComplexe(int echangesJoueur1, int echangesJoueur2, string resultat) {

            TennisScore tennisScore = new TennisScore();

            Assert.Equal(resultat, tennisScore.ConversionEchangesScore(echangesJoueur1, echangesJoueur2));

        }


    }
}
