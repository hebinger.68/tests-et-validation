﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetBowling.models
{
    public class Partie
    {

        public Partie() {
            frames.Add(new Frame());
        }


        public List<Frame> frames = new List<Frame>();


        public void AddLancee(int lancee) {

            if (EstPartieTerminee()) {
                throw new Exception("Partie Terminée");
            }


            if (DerniereFrameTerminee()) {
                Frame nouvelleFrame = new Frame();

                frames [frames.Count - 1].frameSuivante = nouvelleFrame;
                frames.Add(nouvelleFrame);

                if (frames.Count >= 10) {
                    nouvelleFrame.estDerniereFrame = true;
                }
            }

            frames [frames.Count - 1].AddLancee(lancee);

        }


        public bool EstPartieTerminee() {

            if (DerniereFrameTerminee()) {

                if (frames.Count == 10 && frames [9].GetState.Equals(Frame.FrameState.TROU)) {
                    return true;
                }

                if (frames.Count == 11) {
                    if (!(frames [9].GetState.Equals(Frame.FrameState.STRIKE)
                        && frames [10].GetState.Equals(Frame.FrameState.STRIKE))) {
                        return true;
                    }
                }
            }

            if (frames.Count == 11
                && frames [9].GetState.Equals(Frame.FrameState.SPARE)
                && frames [10].lancees.Count > 0) {
                return true;
            }

            if (frames.Count == 12 && frames [11].lancees.Count > 0) {
                return true;
            }
            return false;
        }

        private bool DerniereFrameTerminee() {
            return !frames [frames.Count - 1].GetState.Equals(Frame.FrameState.UNDEFINED);
        }

        public int GetScore() {

            int score = 0;

            foreach (Frame frame in frames) {
                score += frame.GetScore();
            }

            return score;

        }



    }
}
