﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetBowling.models
{
    public class Frame
    {

        public bool estDerniereFrame = false;

        public Frame frameSuivante;

        public List<int> lancees = new List<int>();

        private FrameState state = FrameState.UNDEFINED;

        public FrameState GetState { get { return state; } }

        public void AddLancee(int number) {

            if (number < 0 || number > 10) {
                throw new Exception("number out of range");
            }

            if (ScoreLancee() == 10) {
                throw new Exception("no more points to make !");
            }

            if (ScoreLancee() + number > 10) {
                throw new Exception("number invalid");
            }

            lancees.Add(number);

            UpdateState();

        }

        private void UpdateState() {

            if (lancees.Count == 0) {
                state = FrameState.UNDEFINED;
            } else if (lancees.Count == 1) {
                if (lancees [0] == 10) {
                    state = FrameState.STRIKE;
                } else {
                    state = FrameState.UNDEFINED;
                }
            } else if (lancees.Count == 2) {

                if (ScoreLancee() == 10) {
                    state = FrameState.SPARE;
                } else {
                    state = FrameState.TROU;
                }
            }
        }

        public int GetScore() {

            int score = ScoreLancee();

            if (estDerniereFrame) {
                return score;
            }

            if (this.state.Equals(FrameState.SPARE)) {
                score += CalculScoreSpare();
            } else if (this.state.Equals(FrameState.STRIKE)) {
                score += CalculScoreStrike();
            }

            return score;
        }

        private int CalculScoreStrike() {
            int score = 0;
            if (frameSuivante != null && frameSuivante.lancees.Count > 0) {
                score += frameSuivante.lancees [0];

                if (frameSuivante.lancees.Count > 1) {
                    score += frameSuivante.lancees [1];
                } else if (frameSuivante.frameSuivante != null && frameSuivante.frameSuivante.lancees.Count > 0) {
                    score += frameSuivante.frameSuivante.lancees [0];
                }

            }

            return score;
        }

        private int CalculScoreSpare() {
            int score = 0;
            if (frameSuivante != null) {
                if (frameSuivante.lancees.Count > 0) {
                    score += frameSuivante.lancees [0];
                }
            }

            return score;
        }

        private int ScoreLancee() {
            int score = 0;
            foreach (int lancee in lancees) {
                score += lancee;
            }
            return score;
        }

        public enum FrameState
        {
            UNDEFINED,
            TROU,
            SPARE,
            STRIKE
        }
    }
}
