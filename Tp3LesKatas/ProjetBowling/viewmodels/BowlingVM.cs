﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetBowling.viewmodels
{
    public class BowlingVM : ABaseVM
    {

        private IEnumerable<LigneTableauScores> _tableauScores = new List<LigneTableauScores>();

        public IEnumerable<LigneTableauScores> TableauScores
        {
            get { return _tableauScores; }
            set {
                _tableauScores = value;
                NotifyPropertyChanged();
            }
        }


        private int _nombre = 0;
        public int Nombre
        {
            get { return _nombre; }
            set {
                _nombre = value;
                NotifyPropertyChanged();
            }
        }


        private string _message = "";
        public string Message
        {
            get { return _message; }
            set {
                _message = value;
                NotifyPropertyChanged();
            }
        }

        public struct LigneTableauScores
        {

            public string Nombre { get; set; }

            public string Score { get; set; }

            public string Lance1 { get; set; }

            public string Lance2 { get; set; }

            public string Etat { get; set; }

            public string Total { get; set; }

        }



    }
}
