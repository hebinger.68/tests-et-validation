﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace ProjectFizzBuzz.viewmodels
{
    public class FizzBuzzVM : ABaseVM
    {


        private int _choixNombre = 0;

        public int ChoixNombre
        {
            get { return _choixNombre; }
            set {
                _choixNombre = value;
                NotifyPropertyChanged();
            }
        }





        private string _resultat = "";

        public string Resultat
        {
            get { return _resultat; }
            set {
                _resultat = value;
                NotifyPropertyChanged();
            }
        }

    }
}
