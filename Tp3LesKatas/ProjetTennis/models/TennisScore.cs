﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetTennis.models
{
    public class TennisScore
    {

        public string ConversionEchangesScore(int echangeJoueur1, int echangeJoueur2) {

            TestScoreValide(echangeJoueur1);
            TestScoreValide(echangeJoueur2);

            string scoreJoueur1 = ConversionEchangesScoreSimple(echangeJoueur1);
            string scoreJoueur2 = ConversionEchangesScoreSimple(echangeJoueur2);

            scoreJoueur1 = ConversionComplexe(echangeJoueur1, echangeJoueur2, scoreJoueur1);
            scoreJoueur2 = ConversionComplexe(echangeJoueur2, echangeJoueur1, scoreJoueur2);

            return scoreJoueur1 + "|" + scoreJoueur2;
        }

        private void TestScoreValide(int echangeJoueur) {
            if (echangeJoueur < 0) {
                throw new Exception("Out of range");
            }

        }

        private static string ConversionComplexe(int echangeJoueur, int echangeJoueurAutre, string scoreJoueur1) {
            if (echangeJoueur >= 4) {
                if (echangeJoueur - echangeJoueurAutre == 1) {
                    scoreJoueur1 = "Av";
                }
                if (echangeJoueur - echangeJoueurAutre >= 2) {
                    scoreJoueur1 = "Victoire";
                }
            }

            return scoreJoueur1;
        }

        private string ConversionEchangesScoreSimple(int echanges) {
            switch (echanges) {
                case 0:
                    return "0";
                case 1:
                    return "15";
                case 2:
                    return "30";
                default:
                    return "40";

            }
        }







    }
}
