﻿using ProjetTennis.models;
using ProjetTennis.viewmodels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjetTennis
{

    public partial class MainWindow : Window
    {

        TennisScoreVM model = new TennisScoreVM();


        public MainWindow() {
            InitializeComponent();

            this.DataContext = model;
        }

        private void buttonPlusJoueur1_Click(object sender, RoutedEventArgs e) {
            model.EchangesJoueur1++;
            MiseAJourScore();

        }



        private void buttonPlusJoueur2_Click(object sender, RoutedEventArgs e) {
            model.EchangesJoueur2++;
            MiseAJourScore();
        }

        private void textBoxJoueur1_TextChanged(object sender, TextChangedEventArgs e) {
            int valeur;
            if (int.TryParse((sender as TextBox).Text, out valeur)) {
                model.EchangesJoueur1 = valeur;
            } else {
                model.Message = "Valeur n'est pas un nombre";
            }
            MiseAJourScore();

        }

        private void textBoxJoueur2_TextChanged(object sender, TextChangedEventArgs e) {
            int valeur;
            if (int.TryParse((sender as TextBox).Text, out valeur)) {
                model.EchangesJoueur2 = valeur;
            } else {
                model.Message = "Valeur n'est pas un nombre";
            }
            MiseAJourScore();
        }

        private void MiseAJourScore() {
            TennisScore tennisScore = new TennisScore();
            try {
                model.Score = tennisScore.ConversionEchangesScore(model.EchangesJoueur1, model.EchangesJoueur2);
            } catch (Exception exception) {
                model.Message = exception.Message;
            }
        }

    }
}
