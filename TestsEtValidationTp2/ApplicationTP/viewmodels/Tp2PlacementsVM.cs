﻿using ApplicationTP.models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace ApplicationTP.viewmodels
{
    public class Tp2PlacementsVM : ABaseVM
    {


        private IEnumerable<string> _choixAbondement = new String [] { };

        public IEnumerable<string> ChoixAbondement
        {
            get { return _choixAbondement; }
            set {
                _choixAbondement = value;
                NotifyPropertyChanged();
            }
        }


        private IEnumerable<string> _choixLivret = new String [] { };

        public IEnumerable<string> ChoixLivret
        {
            get { return _choixLivret; }
            set {
                _choixLivret = value;
                NotifyPropertyChanged();
            }
        }





        private string _choixAbondementSelected = "";

        public string ChoixAbondementSelected
        {
            get { return _choixAbondementSelected; }
            set {
                _choixAbondementSelected = value;
                NotifyPropertyChanged();
            }
        }

        private string _choixLivretSelected = "LivretA";

        public string ChoixlivretSelected
        {
            get { return _choixLivretSelected; }
            set {
                _choixLivretSelected = value;
                NotifyPropertyChanged();
            }
        }


        private float _capitalInitial = 0;

        public float CapitalInitial
        {
            get { return _capitalInitial; }
            set {
                _capitalInitial = value;
                NotifyPropertyChanged();
            }
        }




        private IEnumerable<LigneTableauPlacement> _tableauPlacement = new List<LigneTableauPlacement>();

        public IEnumerable<LigneTableauPlacement> TableauPlacement
        {
            get { return _tableauPlacement; }
            set {
                _tableauPlacement = value;
                NotifyPropertyChanged();
            }
        }


        private float _montantAbondement = 0;

        public float MontantAbondement
        {
            get { return _montantAbondement; }
            set {
                _montantAbondement = value;
                NotifyPropertyChanged();
            }
        }



        private int _anneesCalculees = 1;

        public int AnneesCalculees
        {
            get { return _anneesCalculees; }
            set {
                _anneesCalculees = value;
                NotifyPropertyChanged();
            }
        }


        private IEnumerable<string> _choixAffichage = new String [] { };

        public IEnumerable<string> ChoixAffichage
        {
            get { return _choixAffichage; }
            set {
                _choixAffichage = value;
                NotifyPropertyChanged();
            }
        }

        private string _choixAffichageSelected = "BiMensuel";

        public string ChoixAffichageSelected
        {
            get { return _choixAffichageSelected; }
            set {
                _choixAffichageSelected = value;
                NotifyPropertyChanged();
            }
        }





    }
}
