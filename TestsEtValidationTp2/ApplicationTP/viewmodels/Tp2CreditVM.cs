﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace ApplicationTP.viewmodels
{
    public class Tp2CreditVM : ABaseVM
    {


        private IEnumerable<string> _choixTaux = new String [] { };

        public IEnumerable<string> ChoixTaux
        {
            get { return _choixTaux; }
            set {
                _choixTaux = value;
                NotifyPropertyChanged();
            }
        }

        private string _choixTauxSelected = "Bon";

        public string ChoixTauxSelected
        {
            get { return _choixTauxSelected; }
            set {
                _choixTauxSelected = value;
                NotifyPropertyChanged();
            }
        }





        private bool _estSportif = false;

        public bool EstSportif
        {
            get { return _estSportif; }
            set {
                _estSportif = value;
                NotifyPropertyChanged();
            }
        }

        private bool _estFumeur = false;

        public bool EstFumeur
        {
            get { return _estFumeur; }
            set {
                _estFumeur = value;
                NotifyPropertyChanged();
            }
        }


        private bool _estCardiaque = false;

        public bool EstCardiaque
        {
            get { return _estCardiaque; }
            set {
                _estCardiaque = value;
                NotifyPropertyChanged();
            }
        }


        private bool _estIngenieurInformatique = false;

        public bool EstIngenieurInformatique
        {
            get { return _estIngenieurInformatique; }
            set {
                _estIngenieurInformatique = value;
                NotifyPropertyChanged();
            }
        }

        private bool _estPiloteDeChasse = false;

        public bool EstPiloteDeChasse

        {
            get { return _estPiloteDeChasse; }
            set {
                _estPiloteDeChasse = value;
                NotifyPropertyChanged();
            }
        }


        private int _dureeEmprunt = 0;

        public int DureeEmprunt
        {
            get { return _dureeEmprunt; }
            set {
                _dureeEmprunt = value;
                NotifyPropertyChanged();
            }
        }


        private int _capitalEmprunt = 0;

        public int CapitalEmprunt
        {
            get { return _capitalEmprunt; }
            set {
                _capitalEmprunt = value;
                NotifyPropertyChanged();
            }
        }




        private IEnumerable<LigneTableauCredit> _tableauCredit = new List<LigneTableauCredit>();

        public IEnumerable<LigneTableauCredit> TableauCredit
        {
            get { return _tableauCredit; }
            set {
                _tableauCredit = value;
                NotifyPropertyChanged();
            }
        }


        public class LigneTableauCredit
        {



            public float Interets { get; set; }
            public string InteretsToString
            {
                get {
                    return FormatFloat(Interets);
                }
            }

            public float SommeInterets { get; set; }
            public string SommeInteretsToString
            {
                get {
                    return FormatFloat(SommeInterets);
                }
            }

            public float Mensualite { get; set; }
            public string MensualiteToString
            {
                get {
                    return FormatFloat(Mensualite);
                }
            }



            public float Assurance { get; set; }
            public string AssuranceToString
            {
                get {
                    return FormatFloat(Assurance);
                }
            }

            public float AssuranceSomme { get; set; }
            public string AssuranceSommeToString
            {
                get {
                    return FormatFloat(AssuranceSomme);
                }
            }


            public float SommeRembourssement { get; set; }
            public string SommeRembourssementToString
            {
                get {
                    return FormatFloat(SommeRembourssement);
                }
            }

            public float TauxInterets { get; set; }
            public string TauxInteretsToString
            {
                get {
                    return FormatFloatEnPourcent(TauxInterets);
                }
            }

            public float TauxAssurence { get; set; }
            public string TauxAssurenceToString
            {
                get {
                    return FormatFloatEnPourcent(TauxAssurence);
                }
            }

            public int Annnee { get; set; }


            private string FormatFloat(float f)
            {
                string specifier = "F";
                CultureInfo culture = CultureInfo.CreateSpecificCulture("fr-FR");
                return f.ToString(specifier, culture);
            }
            private string FormatFloatEnPourcent(float f)
            {
                string specifier = "P";
                CultureInfo culture = CultureInfo.CreateSpecificCulture("fr-FR");
                return f.ToString(specifier, culture);
            }



        }




    }
}
