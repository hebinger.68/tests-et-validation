﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationTP.models
{
    public class TauxCreditImmobilier : ITauxCredit
    {

        public float Taux7Ans;
        public float Taux10Ans;
        public float Taux15Ans;
        public float Taux20Ans;
        public float Taux25Ans;

        public TauxCreditImmobilier(float taux7Ans, float taux10Ans, float taux15Ans, float taux20Ans, float taux25Ans)
        {
            Taux7Ans = taux7Ans;
            Taux10Ans = taux10Ans;
            Taux15Ans = taux15Ans;
            Taux20Ans = taux20Ans;
            Taux25Ans = taux25Ans;
        }

        public float TauxParAnnee(int annee)
        {
            if (annee < 10)
            {
                return Taux7Ans;
            }
            else if (annee < 15)
            {
                return Taux10Ans;
            }
            else if (annee < 20)
            {
                return Taux15Ans;
            }
            else if (annee < 25)
            {
                return Taux20Ans;
            }
            else
            {
                return Taux25Ans;
            }
        }
    }
}
