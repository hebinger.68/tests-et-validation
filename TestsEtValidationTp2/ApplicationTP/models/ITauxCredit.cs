﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationTP.models
{
    public interface ITauxCredit
    {
        public float TauxParAnnee(int annee);
    }
}
