﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationTP.models
{
    public class EpargneLogement : PlacementAvecPlafond
    {

        public DateTime dateDebut;

        public const int NB_ANNEE_BLOQUE = 4;
        public const int NB_ANNEE_OUVERT = 10;
        public const int NB_ANNEE_INTERETS = 15;

        public EpargneLogement(Placement placement, float plafond) : base(placement, plafond)
        {
            this.dateDebut = Placement.Date;
        }

        public override float CalculAbondement(float somme)
        {

            if (Placement.Date.Year - dateDebut.Year < NB_ANNEE_BLOQUE || Placement.Date.Year - dateDebut.Year >= NB_ANNEE_BLOQUE + NB_ANNEE_OUVERT)
            {
                //impossible avec ce plan
                return 0;
            }

            return base.CalculAbondement(somme);

        }

        public override float CalculInteretParQuinzaine()
        {

            if (Placement.Date.Year - dateDebut.Year >= NB_ANNEE_INTERETS)
            {
                return 0;
            }

            return base.CalculInteretParQuinzaine();
        }



    }
}
