﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace ApplicationTP.models
{
    public class LigneTableauPlacement
    {

        public LigneTableauPlacement()
        {
            Capital = 0;
            Interets = 0;
            SommeInterets = 0;
        }

        public DateTime Date { get; set; }

        public string DateToString
        {
            get {
                return Date.Year + "/" + Date.Month + "/" + Date.Day;
            }
        }

        public float Capital { get; set; }
        public string CapitalToString
        {
            get {
                return FormatFloat(Capital);
            }
        }


        public float CapitalFinQuinzaine { get; set; }
        public string CapitalFinQuinzaineToString
        {
            get {
                return FormatFloat(CapitalFinQuinzaine);
            }
        }


        public float SommeAbondement { get; set; }
        public string SommeAbondementToString
        {
            get {
                return FormatFloat(SommeAbondement);
            }
        }


        public float Interets { get; set; }
        public string InteretsToString
        {
            get {
                return FormatFloat(Interets);
            }
        }

        public float SommeInterets { get; set; }
        public string SommeInteretsToString
        {
            get {
                return FormatFloat(SommeInterets);
            }
        }


        public float SommeInteretsGlobal { get; set; }
        public string SommeInteretsGlobalToString
        {
            get {
                return FormatFloat(SommeInteretsGlobal);
            }
        }


        private string FormatFloat(float f)
        {
            string specifier = "F";
            CultureInfo culture = CultureInfo.CreateSpecificCulture("fr-FR");
            return f.ToString(specifier, culture);
        }

    }
}
