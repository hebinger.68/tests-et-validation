﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ApplicationTP.models
{
    public class PlacementAvecPlafond
    {

        protected Placement Placement;
        protected float Plafond;

        public PlacementAvecPlafond(Placement placement, float plafond)
        {
            this.Placement = placement;
            this.Plafond = plafond;

        }

        public virtual float CalculAbondement(float somme)
        {
            if (Placement.Capital + somme > Plafond)
            {
                //la somme depasse le plafond
                if (Plafond - Placement.Capital > 0)
                {
                    //on a pas encore atteint le plafond
                    return (Plafond - Placement.Capital);
                }
                else
                {
                    //on depasse deja le plafond
                    return 0;
                }
            }

            //cas normal
            return somme;

        }

        public float Abondement(float somme)
        {
            if (somme > 0 && Placement.Capital + somme > Plafond)
            {
                throw new Exception("dépassement de Plafond !");
            }

            return somme;
        }

        public virtual float CalculInteretParQuinzaine()
        {

            if (Placement.Capital > Plafond)
            {
                //on depasse le plafond
                return (Plafond * Placement.Taux) / 24;
            }

            //cas normal
            return (Placement.Capital * Placement.Taux) / 24;
        }

        public List<LigneTableauPlacement> CalculAnnee()
        {
            List<LigneTableauPlacement> tableau = new List<LigneTableauPlacement>();

            for (int mois = 1; mois <= 12; mois++)
            {
                for (int jour = 1; jour <= 16; jour += 15)
                {

                    LigneTableauPlacement ligneTableauPlacement = new LigneTableauPlacement();

                    ligneTableauPlacement.Date = new DateTime(Placement.Date.Year, mois, jour);

                    ligneTableauPlacement.Capital = Placement.Capital;

                    QuinzaineAbondement(mois, jour, ligneTableauPlacement);


                    float interets = CalculInteretParQuinzaine();

                    ligneTableauPlacement.Interets = interets;

                    //somme depuis le debut de l'annee
                    ligneTableauPlacement.SommeInterets = tableau.Sum(d => d.Interets);
                    ligneTableauPlacement.SommeInterets += interets;

                    if (jour == 16 && mois == 12)
                    {
                        Placement.Capital += ligneTableauPlacement.SommeInterets;
                    }

                    ligneTableauPlacement.CapitalFinQuinzaine = Placement.Capital;

                    tableau.Add(ligneTableauPlacement);
                }
            }

            Placement.Date = new DateTime(Placement.Date.Year + 1, 1, 1);

            return tableau;
        }

        protected void QuinzaineAbondement(int mois, int jour, LigneTableauPlacement ligneTableauPlacement)
        {
            float sommeAbondement = 0;

            if (mois == 1 && jour == 1)
            {
                sommeAbondement += Abondement(CalculAbondement(Placement.AbondementAnnuel));
            }

            if ((mois == 1 || mois == 4 || mois == 7 || mois == 10) && jour == 1)
            {
                sommeAbondement += Abondement(CalculAbondement(Placement.AbondementTrimestriel));
            }

            if (jour == 1)
            {
                sommeAbondement += Abondement(CalculAbondement(Placement.AbondementMensuel));
            }

            ligneTableauPlacement.SommeAbondement = sommeAbondement;

            Placement.Capital += sommeAbondement;
        }
    }
}
