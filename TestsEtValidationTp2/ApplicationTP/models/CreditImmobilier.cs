﻿using ApplicationTP.viewmodels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ApplicationTP.models
{
    public class CreditImmobilier
    {
        public float CapitalEmprunte;
        public ITauxCredit TauxCredit;
        public int DureeEnMois;

        public const int DUREE_ANNES_MIN = 9;
        public const int DUREE_ANNES_MAX = 25;
        public const int CAPITAL_MINI = 50000;

        public CreditImmobilier(float capitalEmprunte, ITauxCredit tauxCredit, int dureeEnMois)
        {
            this.CapitalEmprunte = capitalEmprunte;
            this.TauxCredit = tauxCredit;
            this.DureeEnMois = dureeEnMois;
        }

        public float Interet()
        {
            return (CapitalEmprunte * (TauxAnnee() / 12)) /
                (1 - MathF.Pow((1 + (TauxAnnee() / 12)), (-DureeEnMois)));
        }

        public float TauxAnnee()
        {
            return TauxCredit.TauxParAnnee(DureeEnAnnees(DureeEnMois));
        }

        public bool EstValide()
        {
            if (CapitalEmprunte <= CAPITAL_MINI)
            {
                return false;
            }

            if (DureeEnMois < DUREE_ANNES_MIN * 12 || DureeEnMois > DUREE_ANNES_MAX * 12)
            {
                return false;
            }

            return true;
        }

        public int DureeEnAnnees(int dureeEnMois)
        {
            float dureeAnneesF = (float)dureeEnMois / 12;

            return (int)dureeAnneesF + ((dureeAnneesF > (int)dureeAnneesF) ? 1 : 0); //arrondi superieur
        }

        public float AssuranceMensuel(TauxAssurance tauxAssurance)
        {
            return CapitalEmprunte * tauxAssurance.Calcul();
        }

        public float AssuranceTotal(TauxAssurance tauxAssurance)
        {
            return AssuranceMensuel(tauxAssurance) * DureeEnMois;
        }


        public List<Tp2CreditVM.LigneTableauCredit> GenererTableau(CreditImmobilier creditImmobilier, TauxAssurance tauxAssurance, int dureeEmprunt)
        {
            List<Tp2CreditVM.LigneTableauCredit> tableauCredits = new List<Tp2CreditVM.LigneTableauCredit>();

            for (int i = 0; i < dureeEmprunt; i++)
            {
                Tp2CreditVM.LigneTableauCredit ligneTableauCredit = new Tp2CreditVM.LigneTableauCredit();

                ligneTableauCredit.Interets = creditImmobilier.Interet();
                ligneTableauCredit.Assurance = creditImmobilier.AssuranceMensuel(tauxAssurance);

                ligneTableauCredit.Mensualite = ligneTableauCredit.Interets + ligneTableauCredit.Assurance;

                ligneTableauCredit.TauxInterets = creditImmobilier.TauxAnnee();
                ligneTableauCredit.TauxAssurence = tauxAssurance.Calcul();

                ligneTableauCredit.Annnee = creditImmobilier.DureeEnAnnees(i + 1);

                tableauCredits.Add(ligneTableauCredit);

                ligneTableauCredit.SommeInterets = tableauCredits.Sum(d => d.Interets);
                ligneTableauCredit.AssuranceSomme = tableauCredits.Sum(d => d.Assurance);
                ligneTableauCredit.SommeRembourssement = tableauCredits.Sum(d => d.Mensualite);


            }

            return tableauCredits;
        }

    }
}
