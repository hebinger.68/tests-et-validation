﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationTP.models
{
    public class TauxAssurance
    {
        private const float TAUX_BASE = 0.3f; //%
        private const float TAUX_SPORTIF = -0.05f;//%
        private const float TAUX_FUMEUR = 0.15f;//%
        private const float TAUX_CARDIAQUE = 0.3f;//%
        private const float TAUX_INGENIEUR_INFORMATIQUE = -0.05f;//%
        private const float TAUX_PILOTEDECHASSE = 0.15f;//%
        public bool EstSportif;
        public bool EstFumeur;
        public bool EstCardiaque;
        public bool EstIngenieurInformatique;
        public bool EstPiloteDeChasse;

        public float Taux;

        public TauxAssurance(bool estSportif, bool estFumeur, bool estCardiaque, bool estIngenieurInformatique, bool estPiloteDeChasse)
        {
            EstSportif = estSportif;
            EstFumeur = estFumeur;
            EstCardiaque = estCardiaque;
            EstIngenieurInformatique = estIngenieurInformatique;
            EstPiloteDeChasse = estPiloteDeChasse;

            Taux = Calcul();
        }


        public float Calcul()
        {
            float taux = TAUX_BASE;

            if (EstSportif)
            {
                taux += TAUX_SPORTIF;
            }

            if (EstFumeur)
            {
                taux += TAUX_FUMEUR;
            }

            if (EstCardiaque)
            {
                taux += TAUX_CARDIAQUE;
            }

            if (EstIngenieurInformatique)
            {
                taux += TAUX_INGENIEUR_INFORMATIQUE;
            }

            if (EstPiloteDeChasse)
            {
                taux += TAUX_PILOTEDECHASSE;
            }

            return taux / 100; //passage de % en decimal
        }

    }
}
