﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationTP.models
{
    public class Placement
    {

        public Placement(float capitalInitial, float taux)
        {
            this.Capital = capitalInitial;
            this.Taux = taux;

            this.Date = new DateTime(2021, 1, 1);
        }

        public float Capital { get; set; }
        public float Taux { get; set; }

        public float AbondementAnnuel { get; set; }
        public float AbondementMensuel { get; set; }
        public float AbondementTrimestriel { get; set; }

        public DateTime Date { get; set; }


    }
}
