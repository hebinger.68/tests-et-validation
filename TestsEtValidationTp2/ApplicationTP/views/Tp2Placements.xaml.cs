﻿using ApplicationTP.models;
using ApplicationTP.viewmodels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Linq;

namespace ApplicationTP.views
{
    /// <summary>
    /// Logique d'interaction pour Tp1.xaml
    /// </summary>
    public partial class Tp2Placements : Page
    {

        Tp2PlacementsVM viewModel = new Tp2PlacementsVM();

        public Tp2Placements()
        {
            InitializeComponent();

            this.DataContext = viewModel;

            viewModel.ChoixAbondement = new String [] { "Mensuel", "Trimestriel", "Annuel" };
            viewModel.ChoixLivret = new String [] { "LivretA", "LDD", "EpargneLogement", "Houdini" };
            viewModel.ChoixAffichage = new String [] { "BiMensuel", "Annuel" };
        }



        private void BoutonCalculer_Click(object sender, RoutedEventArgs e)
        {

            Placement placement;
            PlacementAvecPlafond placementAvecPlafond;

            switch (viewModel.ChoixlivretSelected)
            {
                case "LivretA":
                    placement = new Placement((viewModel.CapitalInitial), 0.0075f);
                    placementAvecPlafond = new PlacementAvecPlafond(placement, 22950);
                    break;
                case "LDD":
                    placement = new Placement((viewModel.CapitalInitial), 0.005f);
                    placementAvecPlafond = new PlacementAvecPlafond(placement, 12000);
                    break;
                case "EpargneLogement":
                    placement = new Placement((viewModel.CapitalInitial), 0.01f);
                    placementAvecPlafond = new EpargneLogement(placement, 61200);
                    break;
                case "Houdini":
                    placement = new Placement((viewModel.CapitalInitial), 0.08f);
                    placementAvecPlafond = new PlacementAvecPlafond(placement, float.MaxValue);
                    break;
                default:
                    placement = new Placement((viewModel.CapitalInitial), 0);
                    placementAvecPlafond = new PlacementAvecPlafond(placement, 0);
                    break;
            }



            AjoutAbondement(placement);


            List<LigneTableauPlacement> ligneTableauPlacements = new List<LigneTableauPlacement>();
            GenererTableau(placementAvecPlafond, ligneTableauPlacements);

            viewModel.TableauPlacement = ligneTableauPlacements;

        }

        private void GenererTableau(PlacementAvecPlafond placementAvecPlafond, List<LigneTableauPlacement> ligneTableauPlacements)
        {
            for (int i = 0; i < viewModel.AnneesCalculees; i++)
            {
                switch (viewModel.ChoixAffichageSelected)
                {
                    case "Annuel":
                        List<LigneTableauPlacement> nouvelleLignesTableauPlacements = placementAvecPlafond.CalculAnnee().OrderBy(d => d.Date).ToList();

                        LigneTableauPlacement ligneAnnuelle = nouvelleLignesTableauPlacements.First();
                        ligneAnnuelle.SommeAbondement = nouvelleLignesTableauPlacements.Sum(d => d.SommeAbondement);
                        ligneAnnuelle.CapitalFinQuinzaine = nouvelleLignesTableauPlacements [23].CapitalFinQuinzaine;
                        ligneAnnuelle.Date = nouvelleLignesTableauPlacements [23].Date;

                        ligneTableauPlacements.Add(ligneAnnuelle);
                        break;

                    case "BiMensuel":
                        ligneTableauPlacements.AddRange(placementAvecPlafond.CalculAnnee());
                        break;

                    default:
                        //rien selectionne
                        break;
                }

            }
        }

        private void AjoutAbondement(Placement placement)
        {
            switch (viewModel.ChoixAbondementSelected)
            {
                case "Annuel":
                    placement.AbondementAnnuel = (viewModel.MontantAbondement);
                    break;

                case "Mensuel":
                    placement.AbondementMensuel = (viewModel.MontantAbondement);
                    break;

                case "Trimestriel":
                    placement.AbondementTrimestriel = (viewModel.MontantAbondement);
                    break;

                default:
                    //rien selectionne
                    break;
            }
        }

        private void DataGrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString();
        }
    }
}
