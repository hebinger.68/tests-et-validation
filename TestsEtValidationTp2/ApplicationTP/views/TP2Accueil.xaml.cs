﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ApplicationTP.views
{
    /// <summary>
    /// Logique d'interaction pour TP2Accueil.xaml
    /// </summary>
    public partial class TP2Accueil : Page
    {
        public TP2Accueil()
        {
            InitializeComponent();
        }

        private void BoutonPlacement_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Tp2Placements());
        }

        private void BoutonCredit_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Tp2Credit());
        }
    }
}
