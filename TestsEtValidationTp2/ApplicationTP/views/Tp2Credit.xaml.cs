﻿using ApplicationTP.models;
using ApplicationTP.viewmodels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Linq;

namespace ApplicationTP.views
{
    /// <summary>
    /// Logique d'interaction pour Tp2Credit.xaml
    /// </summary>
    public partial class Tp2Credit : Page
    {

        Tp2CreditVM viewModel = new Tp2CreditVM();

        public Tp2Credit()
        {
            InitializeComponent();

            this.DataContext = viewModel;

            viewModel.ChoixTaux = new String [] { "Bon", "TresBon", "Excellent" };
        }

        private void BoutonCalculer_Click(object sender, RoutedEventArgs e)
        {

            ITauxCredit tauxCredit;
            tauxCredit = GenererTaux();

            CreditImmobilier creditImmobilier = new CreditImmobilier(viewModel.CapitalEmprunt, tauxCredit, viewModel.DureeEmprunt);

            TauxAssurance tauxAssurance = new TauxAssurance(viewModel.EstSportif, viewModel.EstFumeur, viewModel.EstCardiaque, viewModel.EstIngenieurInformatique, viewModel.EstPiloteDeChasse);

            viewModel.TableauCredit = creditImmobilier.GenererTableau(creditImmobilier, tauxAssurance, viewModel.DureeEmprunt);

        }



        public ITauxCredit GenererTaux()
        {
            ITauxCredit tauxCredit;
            switch (viewModel.ChoixTauxSelected)
            {
                case "Bon":
                    tauxCredit = new TauxCreditImmobilier(0.0062f, 0.0067f, 0.0085f, 0.0104f, 0.0127f);
                    break;
                case "TresBon":
                    tauxCredit = new TauxCreditImmobilier(0.0042f, 0.0055f, 0.0073f, 0.0091f, 0.0115f);
                    break;
                case "Excellent":
                    tauxCredit = new TauxCreditImmobilier(0.0035f, 0.0045f, 0.0058f, 0.0073f, 0.0089f);
                    break;


                default:
                    tauxCredit = new TauxCreditImmobilier(0, 0, 0, 0, 0);
                    break;
            }

            return tauxCredit;
        }
    }
}
