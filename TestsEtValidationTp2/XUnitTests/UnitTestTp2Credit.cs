﻿using System;
using System.Collections.Generic;
using System.Text;
using ApplicationTP.models;

using ApplicationTP.viewmodels;
using Xunit;
using System.Linq;

namespace XUnitTests
{
    public class UnitTestTp2Credit
    {


        [Theory]
        [InlineData(12, 1)]
        [InlineData(15, 2)]
        [InlineData(9, 1)]
        [InlineData(1, 1)]
        [InlineData(0, 0)]
        public void TestCreditRoundInt(int mois, int resultat)
        {
            ITauxCredit tauxCredit = new TauxCreditImmobilier(0.0062f, 0.0067f, 0.0085f, 0.0104f, 0.0127f);

            CreditImmobilier creditImmobilier = new CreditImmobilier(0, tauxCredit, 0);

            int reponse = creditImmobilier.DureeEnAnnees(mois);

            Assert.Equal(resultat, reponse);

        }


        [Theory]
        [InlineData(6, 0.0062f)]
        [InlineData(12, 0.0067f)]
        [InlineData(17, 0.0085f)]
        [InlineData(22, 0.0104f)]
        [InlineData(25, 0.0127f)]
        public void TestCreditTaux(int annee, float resultat)
        {
            ITauxCredit tauxCredit = new TauxCreditImmobilier(0.0062f, 0.0067f, 0.0085f, 0.0104f, 0.0127f);

            Assert.Equal(resultat, tauxCredit.TauxParAnnee(annee));

        }


        [Theory]
        [InlineData(100000, 10, "10028,73")]
        public void TestCreditInterets(float capital, int dureeEnMois, string resultat)
        {
            ITauxCredit tauxCredit = new TauxCreditImmobilier(0.0062f, 0.0067f, 0.0085f, 0.0104f, 0.0127f);

            CreditImmobilier creditImmobilier = new CreditImmobilier(capital, tauxCredit, dureeEnMois);

            TauxAssurance tauxAssurance = new TauxAssurance(false, false, false, false, false);

            List<Tp2CreditVM.LigneTableauCredit> tableau = creditImmobilier.GenererTableau(creditImmobilier, tauxAssurance, dureeEnMois);

            Assert.Equal(resultat, tableau.First().InteretsToString);

        }


        [Theory]
        [InlineData(100000, 10, "10328,73")]
        public void TestCreditMensualite(float capital, int dureeEnMois, string resultat)
        {
            ITauxCredit tauxCredit = new TauxCreditImmobilier(0.0062f, 0.0067f, 0.0085f, 0.0104f, 0.0127f);

            CreditImmobilier creditImmobilier = new CreditImmobilier(capital, tauxCredit, dureeEnMois);

            TauxAssurance tauxAssurance = new TauxAssurance(false, false, false, false, false);

            List<Tp2CreditVM.LigneTableauCredit> tableau = creditImmobilier.GenererTableau(creditImmobilier, tauxAssurance, dureeEnMois);

            Assert.Equal(resultat, tableau.First().MensualiteToString);

        }


        [Theory]
        [InlineData(100000, 10, "300,00")]
        public void TestCreditAssurance(float capital, int dureeEnMois, string resultat)
        {
            ITauxCredit tauxCredit = new TauxCreditImmobilier(0.0062f, 0.0067f, 0.0085f, 0.0104f, 0.0127f);

            CreditImmobilier creditImmobilier = new CreditImmobilier(capital, tauxCredit, dureeEnMois);

            TauxAssurance tauxAssurance = new TauxAssurance(false, false, false, false, false);

            List<Tp2CreditVM.LigneTableauCredit> tableau = creditImmobilier.GenererTableau(creditImmobilier, tauxAssurance, dureeEnMois);

            Assert.Equal(resultat, tableau.First().AssuranceToString);

        }

        [Theory]
        [InlineData(100000, 10, "103287,26")]
        public void TestCreditSommeRembourcement(float capital, int dureeEnMois, string resultat)
        {
            ITauxCredit tauxCredit = new TauxCreditImmobilier(0.0062f, 0.0067f, 0.0085f, 0.0104f, 0.0127f);

            CreditImmobilier creditImmobilier = new CreditImmobilier(capital, tauxCredit, dureeEnMois);

            TauxAssurance tauxAssurance = new TauxAssurance(false, false, false, false, false);

            List<Tp2CreditVM.LigneTableauCredit> tableau = creditImmobilier.GenererTableau(creditImmobilier, tauxAssurance, dureeEnMois);

            Assert.Equal(resultat, tableau.OrderByDescending(d => d.SommeRembourssement).First().SommeRembourssementToString);

        }


        [Theory]
        [InlineData(false, false, false, false, false, 0.003)]
        [InlineData(true, false, false, false, false, 0.0025)]
        [InlineData(true, false, false, false, true, 0.004)]
        [InlineData(true, true, true, true, true, 0.008)]

        public void TestTauxAssurance(bool sportif, bool fumeur, bool cardiaque, bool ingenieur, bool pilote, float resultat)
        {
            TauxAssurance tauxAssurance = new TauxAssurance(sportif, fumeur, cardiaque, ingenieur, pilote);

            Assert.Equal(resultat, tauxAssurance.Calcul());

        }







    }

}
