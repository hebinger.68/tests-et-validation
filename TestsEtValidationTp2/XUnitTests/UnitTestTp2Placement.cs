using System;
using Xunit;
using ApplicationTP.models;
using System.Collections.Generic;
using System.Linq;

namespace XUnitTests
{
    public class UnitTestTp2Placement
    {
        [Theory]
        [InlineData(100, 0.005, 1, 100.5)]
        public void TestPlacementTaux(float capitalInitial, float taux, int annee, float resultat)
        {
            Placement placement = new Placement(capitalInitial, taux);
            PlacementAvecPlafond placementAvecPlafond = new PlacementAvecPlafond(placement, 1000000);

            List<LigneTableauPlacement> tableau = new List<LigneTableauPlacement>();

            for (int i = 0; i < annee; i++)
            {
                tableau.AddRange(placementAvecPlafond.CalculAnnee());
            }

            float capital = tableau.OrderByDescending(d => d.Date).First().CapitalFinQuinzaine;

            Assert.Equal(resultat, capital);

        }



        [Theory]
        [InlineData(100, 20, 1000, 100, 1000)]
        public void TestPlacementPlafond(float capitalInitial, int annee, float plafond, float abondement, float resultat)
        {
            Placement placement = new Placement(capitalInitial, 0);
            PlacementAvecPlafond placementAvecPlafond = new PlacementAvecPlafond(placement, plafond);

            placement.AbondementAnnuel = abondement;

            List<LigneTableauPlacement> tableau = new List<LigneTableauPlacement>();

            for (int i = 0; i < annee; i++)
            {
                tableau.AddRange(placementAvecPlafond.CalculAnnee());
            }

            float capital = tableau.OrderByDescending(d => d.Date).First().CapitalFinQuinzaine;

            Assert.Equal(resultat, capital);

        }



    }
}
